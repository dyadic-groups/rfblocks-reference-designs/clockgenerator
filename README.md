# A Versatile Clock Generator

## Features

- Agile frequency selection to 900 MHz
- CMOS, LVDS or LVPECL clock signals
- Reference switchable between internal and external sources

## Documentation

Full documentation for the Clock Generator reference design is available at
[RF Blocks](https://rfblocks.org/refdesigns/ClockGen/Clock-Gen.html)

